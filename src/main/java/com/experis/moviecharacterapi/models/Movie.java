package com.experis.moviecharacterapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "movieId", scope = Movie.class)
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private Long movieId;
    private String title;
    private String genres;
    @Column(name = "release_year")
    private Integer releaseYear;
    private String director;
    private String picture;
    private String trailer;

    // Movie belongs to one franchise, franchise have many movies
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    public Franchise franchise;

    @ManyToMany(mappedBy = "movies")
    Set<Character> characters = new HashSet<>();

    @JsonGetter("franchise")
    public String franchise(){
        if (franchise == null){
            System.out.println("is null");
            return null;
        }
        return "/api/v1/franchises/"+franchise.getFranchiseId();
    }

    @JsonGetter("characters")
    public Set<String> getCharactersByMovie() {
        return characters.stream().map(character -> "/api/v1/characters/" +
                character.getCharacterId()).collect(Collectors.toSet());
    }

    public boolean addNewCharacter(Character character) {
        return this.characters.add(character);
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public Set<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(Set<Character> characters) {
        this.characters = characters;
    }
}
