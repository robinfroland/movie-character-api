package com.experis.moviecharacterapi.models;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "franchiseId", scope = Franchise.class)
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //
    @Column(name = "franchise_id")
    private Long franchiseId;
    private String name;
    private String description;

    @OneToMany(mappedBy = "franchise")
    Set<Movie> movies = new HashSet<>();

    @JsonGetter("movies")
    public Set<String> movies(){
        return movies.stream()
                .map(movie -> "/api/v1/movies/" + movie.getMovieId()).collect(Collectors.toSet());
    }

    public boolean addNewMovie(Movie movie) {
        return this.movies.add(movie);
    }

    public Long getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(Long franchiseId) {
        this.franchiseId = franchiseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
