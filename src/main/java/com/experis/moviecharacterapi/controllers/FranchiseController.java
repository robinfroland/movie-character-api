package com.experis.moviecharacterapi.controllers;

import com.experis.moviecharacterapi.models.Character;
import com.experis.moviecharacterapi.models.Franchise;
import com.experis.moviecharacterapi.models.Movie;
import com.experis.moviecharacterapi.repositories.FranchiseRepository;
import com.experis.moviecharacterapi.repositories.MovieRepository;
import com.experis.moviecharacterapi.util.BadRequestException;
import com.experis.moviecharacterapi.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchises") //Extending this path
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieRepository movieRepository;

    @GetMapping("/")
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        List<Franchise> franchises = franchiseRepository.findAll();
        return new ResponseEntity<>(franchises, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){
        Franchise reCheracter = new Franchise();
        HttpStatus status;

        if (franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            reCheracter = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(reCheracter, status);
    }

    @PostMapping("/")
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){

        Franchise updatedFranchise = setExistingRelations(franchise);
        franchiseRepository.save(updatedFranchise);
        return new ResponseEntity<>(updatedFranchise, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise){
        Franchise updatedFranchise = new Franchise();

        //Save only if id matches, preventing interception
        if (id.equals(franchise.getFranchiseId())){
            updatedFranchise = setExistingRelations(franchise);
        } else {
            return new ResponseEntity<>(updatedFranchise, HttpStatus.BAD_REQUEST);
        }

        franchiseRepository.save(updatedFranchise);
        return new ResponseEntity<>(updatedFranchise, HttpStatus.OK);
    }

    @GetMapping("/{id}/characters")
    public ResponseEntity<Set<Character>> getAllCharactersInFranchise(@PathVariable Long id) {
        Set<Character> characters = new HashSet<>();
        HttpStatus status;
        if (franchiseRepository.existsById(id)) {
            for (Movie m : franchiseRepository.findById(id).get().getMovies()){
                characters.addAll(m.getCharacters());
            }
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(characters, status);
    }

    @GetMapping("/{id}/movies")
    public ResponseEntity<Set<Movie>> getAllMoviesInFranchise(@PathVariable Long id) {
        Set<Movie> movies = new HashSet<>();
        HttpStatus status;
        if (franchiseRepository.existsById(id)) {
            movies = franchiseRepository.findById(id).get().getMovies();
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movies, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteFranchise(@PathVariable Long id) {
        // Cast exception with http status 'not found' if movie with id does not exist
        Franchise franchise = franchiseRepository.findById(id).orElseThrow(()
                -> new NotFoundException("franchise", id));

        for (Movie movie : franchise.getMovies()) {
            movie.setFranchise(null);
        }

        franchiseRepository.delete(franchise);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private Franchise setExistingRelations(Franchise franchise) {
        if (!franchise.getMovies().isEmpty()){
            for (Movie movie : franchise.getMovies()){
                if (movie.getMovieId() != null){
                    Movie existingMovie = movieRepository.findById(movie.getMovieId())
                            .orElseThrow(() -> new BadRequestException("Provided movie does not exist"));
                    if (movie.franchise != null) {
                        if (movie.getFranchise().getFranchiseId() != franchise.getFranchiseId()) {
                            existingMovie.setFranchise(franchise);
                        }
                    }
                    franchise.addNewMovie(movie);
                }
            }
        }
        return franchise;
    }
}
