package com.experis.moviecharacterapi.controllers;

import com.experis.moviecharacterapi.models.Character;
import com.experis.moviecharacterapi.models.Movie;
import com.experis.moviecharacterapi.repositories.CharacterRepository;
import com.experis.moviecharacterapi.repositories.MovieRepository;
import com.experis.moviecharacterapi.util.BadRequestException;
import com.experis.moviecharacterapi.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters") //Extending this path
public class CharacterController {

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private MovieRepository movieRepository;

    @GetMapping("/")
    public ResponseEntity<List<Character>> getAllCharacters(){
        List<Character> characters = characterRepository.findAll();
        return new ResponseEntity<>(characters, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacter(@PathVariable Long id){
        Character reCheracter = new Character();
        HttpStatus status;

        if (characterRepository.existsById(id)){
            status = HttpStatus.OK;
            reCheracter = characterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(reCheracter, status);
    }

    @PostMapping("/")
    public ResponseEntity<Character> addCharacter(@RequestBody Character character){
        Character returnCharacter;
        returnCharacter = characterRepository.save(character);
        return new ResponseEntity<>(returnCharacter, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character character){
        HttpStatus status;

        Character returnCharacter = new Character();
        //Save only if id matches, preventing interception
        if (id.equals(character.getCharacterId())){
            if (!character.getMovies().isEmpty()) {
                for (Movie movie : character.getMovies()) {
                    if (movie.getMovieId() != null) {
                        Movie existingMovie = movieRepository.findById(movie.getMovieId())
                                .orElseThrow(() -> new BadRequestException("Provided movie does not exist"));

                        boolean existingRelation = false;
                        for (Character c : movie.getCharacters()) {
                            if (c.getCharacterId() == character.getCharacterId()) {
                                existingRelation = true;
                            }
                        }
                        if (!existingRelation) existingMovie.addNewCharacter(character);
                        character.setNewMovie(existingMovie);
                    }
                }
            }
            returnCharacter = characterRepository.save(character);
        }
        return new ResponseEntity<>(returnCharacter, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteCharacter(@PathVariable Long id) {
        // Cast exception with http status 'not found' if character with id does not exist
        Character character = characterRepository.findById(id).orElseThrow(() -> new NotFoundException("character", id));

        // For each movie this character is part of, detach reference to character before deletion
        for (Movie movie : character.getMovies()) {
            movie.getCharacters().remove(character);
        }

        characterRepository.delete(character);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
