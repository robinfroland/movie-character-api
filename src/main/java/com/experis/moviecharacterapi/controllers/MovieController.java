package com.experis.moviecharacterapi.controllers;

import com.experis.moviecharacterapi.models.Character;
import com.experis.moviecharacterapi.models.Franchise;
import com.experis.moviecharacterapi.models.Movie;
import com.experis.moviecharacterapi.repositories.CharacterRepository;
import com.experis.moviecharacterapi.repositories.FranchiseRepository;
import com.experis.moviecharacterapi.repositories.MovieRepository;
import com.experis.moviecharacterapi.util.BadRequestException;
import com.experis.moviecharacterapi.util.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movies") //Extending this path
public class MovieController {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private CharacterRepository characterRepository;

    @GetMapping("/")
    public ResponseEntity<List<Movie>> getAllMovies(){
        List<Movie> movies = movieRepository.findAll();
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable Long id){
        Movie reCheracter = new Movie();
        HttpStatus status;

        if (movieRepository.existsById(id)){
            status = HttpStatus.OK;
            reCheracter = movieRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(reCheracter, status);
    }

    @PostMapping("/")
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        HttpStatus status;
        Movie returnMovie = new Movie();
        Movie updatedMovie = setExistingRelations(movie);

        returnMovie = movieRepository.save(updatedMovie);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnMovie,status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie){
        Movie returnMovie = new Movie();
        HttpStatus status;

        Movie updatedMovie = new Movie();
        //Save only if id matches, preventing interception
        if (id.equals(movie.getMovieId())){
            updatedMovie = setExistingRelations(movie);
        } else {
            status = HttpStatus.BAD_REQUEST; //Illegal edit
            return new ResponseEntity<>(returnMovie,status);
        }

        returnMovie = movieRepository.save(updatedMovie);
        status = HttpStatus.OK;
        return new ResponseEntity<>(returnMovie, status);
    }

    @GetMapping("/{id}/characters")
    public ResponseEntity<Set<Character>> getAllCharactersInMovie(@PathVariable Long id) {
        Set<Character> characters = new HashSet<>();
        HttpStatus status;

        if (movieRepository.existsById(id)) {
            characters = movieRepository.findById(id).get().getCharacters();
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(characters, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteMovie(@PathVariable Long id) {
        // Cast exception with http status 'not found' if movie with id does not exist
        Movie movie = movieRepository.findById(id).orElseThrow(() -> new NotFoundException("movie", id));

        // For each character in movie, detach reference to movie before deletion
        for (Character character : movie.getCharacters()) {
            character.getMovies().remove(movie);
        }
        movieRepository.delete(movie);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private Movie setExistingRelations(Movie movie) {
        if (movie.franchise != null) {
            if (movie.franchise.getFranchiseId() != null) {
                Franchise existingFranchise = franchiseRepository.findById(movie.franchise.getFranchiseId())
                        .orElseThrow(() -> new BadRequestException("Provided franchise not found"));
                movie.setFranchise(existingFranchise);
            }
        }

        // If there is characters in new movie
        // If id provided -> Check if characterId is an existing characterId
        // If not -> BAD REQUEST
        // If no id provided, just add new character
        if (!movie.getCharacters().isEmpty()) {
            Set<Character> returnCharacters = new HashSet<>();
            for (Character character : movie.getCharacters()) {
                if (character.getCharacterId() != null) {
                    Character existingCharacter = characterRepository.findById(character.getCharacterId())
                            .orElseThrow(() -> new BadRequestException("Provided character not found"));

                    boolean exists = false;
                    for (Movie c : existingCharacter.getMovies()) {
                        if (c.getMovieId() == movie.getMovieId()) {
                            exists = true;
                        }
                    }
                    if (!exists) {
                        existingCharacter.setNewMovie(movie);
                    }
                    returnCharacters.add(existingCharacter);

                }
                movie.setCharacters(returnCharacters);
            }
        }
        return movie;
    }
}

