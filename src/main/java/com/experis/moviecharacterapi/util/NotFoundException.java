package com.experis.moviecharacterapi.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

    public NotFoundException(String tableName, Long id){
        super("Could not find " + tableName + " with id: " + id);
    }
}
