# Movie characters API
 
## Project
- Made using Java Spring, Hibernate, PostgreSQL, Heroku.

## Developers 
- Robin Frøland
- Pia Wold Nilsen
- Steinar Valheim Pedersen

## Demo
- Deployed at https://movie-api-experis.herokuapp.com/.
  
# API Endpoints

## Character

#### `GET` READ characters
Returns information about **all** characters.

    /api/v1/characters/

#### `GET` READ character
Returns information about a character, specified by **id** (long).

    /api/v1/characters/{id}

#### `POST` CREATE new character
Accepts a **character object** in request body and inserts new values into the database.

    /api/v1/characters/

#### `PUT` UPDATE existing character
Accepts a **character object** in request body and updates values in the database on provided **id** (long).

    /api/v1/characters/{id}

#### `DELETE` DELETE existing character
Deletes a character, specified by **id** (long).

    /api/v1/characters/{id}


## Movie

#### `GET` READ movies
Returns information about **all** movies.

    /api/v1/movies/

#### `GET` READ movie
Returns information about a movie, specified by **id** (long).

    /api/v1/movies/{id}

#### `GET` READ all characters in this movie
Returns information about all characters who've played in this movie specified by movie **id** (long).

    /api/v1/movies/{id}/characters

#### `POST` CREATE new movie
Accepts a **movie object** in request body and inserts new values into the database.

    /api/v1/movies/

#### `PUT` UPDATE existing movies
Accepts a **movie object** in request body and updates values in the database on provided **id** (long).

    /api/v1/movies/{id}

#### `DELETE` DELETE existing movie
Deletes a movie, specified by **id** (long).

    /api/v1/movies/{id}


## Franchise

#### `GET` READ franchises
Returns information about **all** franchises.

    /api/v1/franchises/

#### `GET` READ franchise
Returns information about a franchise, specified by **id** (long).

    /api/v1/franchises/{id}

#### `GET` READ all movies in this franchise
Returns information about **all** movies in this franchise, specified by franchise **id** (long).

    /api/v1/franchises/{id}/characters/movies

#### `GET` READ all characters in this franchise
Returns information about **all** characters playing a role this franchise, specified by franchise **id** (long).

    /api/v1/franchises/{id}/characters


#### `POST` CREATE franchise
Accepts a **franchise object** in request body and inserts new values into the database.

    /api/v1/franchises/

#### `PUT` UPDATE existing franchise
Accepts a **franchise object** in request body and updates values in the database on provided **id** (long).

    /api/v1/franchises/{id}

#### `DELETE` DELETE existing franchise
Deletes a franchise, specified by **id** (long).

    /api/v1/franchises/{id}